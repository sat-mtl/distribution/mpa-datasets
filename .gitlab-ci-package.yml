variables:
  DOCKER_DRIVER: overlay2

stages:
  - deploy

variables: &mpa-datasets-variables
  MPA_ARCH: "amd64"
  MPA_CPU: "x86_64"
  MPA_DISTRO: "focal"
  MPA_RELEASE: "20.04"
  MPA_CUDA: "11.4"
  MPA_GPU: "nvidia"

.define-variables: &define-variables
  - export DEBIAN_FRONTEND=noninteractive
  - export CUDA_HOME="/usr/local/cuda-${MPA_CUDA}"
  - export PATH="${CUDA_HOME}/bin:${PATH}"
  - export LD_LIBRARY_PATH="${CUDA_HOME}/lib64:${LD_LIBRARY_PATH}"

.generate-variables: &generate-variables
  - export MPA_NAME="mpa-datasets"
  - export MPA_PATH="sat-mtl/distribution/${MPA_NAME}"
  - export MPA_ID=`echo $MPA_PATH | sed 's/\//%2F/g'`
  - |
    export TOKEN="JOB-TOKEN: ${CI_JOB_TOKEN}"
  - |
    export PRIVATE_TOKEN="PRIVATE-TOKEN: ${ACCESS_TOKEN}"
  - export DEB_VERSION=`dpkg-parsechangelog --show-field Version | sed 's/:/-/g'`
  - export DATASETS_ARCHIVE=` echo ${CI_PROJECT_NAME}-${DEB_VERSION}-${MPA_ARCH}-ubuntu${MPA_RELEASE}-cuda${MPA_CUDA}-${MPA_GPU}-datasets.tar.gz`
  - export DATASETS_ARCHIVE_ESCAPED=`echo $DATASETS_ARCHIVE | jq -R -r @uri`

release:mpa-datasets:
  stage: deploy
  image: ubuntu:20.04
  variables:
    <<: *mpa-datasets-variables
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      exists: 
        - debian/changelog
    - if: $CI_COMMIT_BRANCH =~ /^debian.*$/
      exists: 
        - debian/changelog
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
  before_script:
    - echo "Skipping before script from main config."
  script:
    - *define-variables
    # Install dependencies for script
    - apt update -qq
    - DEBIAN_FRONTEND=noninteractive apt install -y --no-install-recommends
      ca-certificates coreutils curl dpkg-dev jq sed
    - *generate-variables
    # Get MPA trigger token
    - |
      MPA_TRIGGERS=`curl --header "${PRIVATE_TOKEN}" "https://gitlab.com/api/v4/projects/${MPA_ID}/triggers"`
    - |
      echo "Found MPA triggers: ${MPA_TRIGGERS}"
    - MPA_TRIGGERS_LENGTH=`echo $MPA_TRIGGERS | jq length`
    - |
      if [ ${MPA_TRIGGERS_LENGTH} -eq 0 ]; \
        then \
        echo "Create MPA trigger: https://gitlab.com/${MPA_PATH}/-/settings/ci_cd#js-pipeline-triggers "; \
        exit 1; \
      fi
    - TRIGGER_TOKEN=`echo $MPA_TRIGGERS | jq .[0].token`
    - |
      if [ ${#TRIGGER_TOKEN} -lt 5 ]; \
        then \
        echo "Recreate MPA trigger by user granting tokens: https://gitlab.com/${MPA_PATH}/-/settings/ci_cd#js-pipeline-triggers "; \
        exit 1; \
      fi
    - |
      PACKAGES=`curl --header "${TOKEN}" \
      "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages"`
    - PACKAGES_LENGTH=`echo ${PACKAGES} | jq length`
    - echo "Found ${PACKAGES_LENGTH} package(s)"
    - |
      PACKAGE_INDEX=0; \
      PACKAGE_ID=0; \
      while [ $PACKAGE_INDEX -lt $((PACKAGES_LENGTH)) ]; \
      do \
        PACKAGE_VERSION=`echo ${PACKAGES} | jq -r .[${PACKAGE_INDEX}].version`; \
        echo "Checking package index ${PACKAGE_INDEX} version ${PACKAGE_VERSION}"; \
        if [[ "${PACKAGE_VERSION}" == "${DEB_VERSION}" ]]; \
          then \
          echo "Found package index ${PACKAGE_INDEX} matching version ${DEB_VERSION}"; \
          PACKAGE_ID=`echo ${PACKAGES} | jq .[${PACKAGE_INDEX}].id`; \
          break; \
        fi; \
        PACKAGE_INDEX=$((${PACKAGE_INDEX} + 1)); \
      done; \
      if [ "${PACKAGE_ID}" -eq "0" ]; \
        then \
        echo "Failed find package for version: ${DEB_VERSION}"; \
        exit 1; \
      fi
    # Get package file id and name
    - |
      PACKAGE_FILES=`curl --header "${TOKEN}" \
      "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/${PACKAGE_ID}/package_files?per_page=100"`
    - PACKAGE_FILES_LENGTH=`echo ${PACKAGE_FILES} | jq length`
    - |
      PACKAGE_FILES_INDEX=0; \
      PACKAGE_FILES_ID=0; \
      PACKAGE_FILES_DATE=""; \
      while [ $PACKAGE_FILES_INDEX -lt $((PACKAGE_FILES_LENGTH)) ]; \
      do \
        PACKAGE_FILE_NAME=`echo ${PACKAGE_FILES} | jq .[${PACKAGE_FILES_INDEX}].file_name`; \
        _PACKAGE_FILES_DATE=`echo ${PACKAGE_FILES} | jq .[${PACKAGE_FILES_INDEX}].created_at`; \
        echo "Checking package index ${PACKAGE_FILES_INDEX} name ${PACKAGE_FILE_NAME}created at ${_PACKAGE_FILES_DATE}"; \
        if [[ "${PACKAGE_FILE_NAME}" == *"${DATASETS_ARCHIVE}"* ]] && [[ "${_PACKAGE_FILES_DATE}" > "${PACKAGE_FILES_DATE}" ]]; \
          then \
          PACKAGE_FILES_DATE=${_PACKAGE_FILES_DATE} \
          echo "Found package file index ${PACKAGE_FILES_INDEX} name ${PACKAGE_FILE_NAME} matching arch ${MPA_ARCH}"; \
          PACKAGE_FILE_ID=`echo ${PACKAGE_FILES} | jq .[${PACKAGE_FILES_INDEX}].id`; \
          #break; \
        fi; \
        PACKAGE_FILES_INDEX=$((${PACKAGE_FILES_INDEX} + 1)); \
      done; \
      if [ "${PACKAGE_FILE_ID}" -eq "0" ]; \
        then \
        echo "Failed find package file for arch: ${MPA_ARCH}"; \
        #exit 1; \
      fi
    - echo "Identified package file ${PACKAGE_FILE_NAME} id ${PACKAGE_FILE_ID} for arch ${MPA_ARCH}"
    # Get or create release by tag name
    - export RELEASE_ID="debian%2F${DEB_VERSION}"
    - |
      RELEASE=`curl --header "${TOKEN}" \
      "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/releases/${RELEASE_ID}"`
    - RELEASE_LENGTH=0
    - |
      if [[ "${RELEASE}" != '{"message"'* ]]; \
        then \
        RELEASE_LENGTH=`echo ${RELEASE} | jq length`; \
        echo "Found release ${RELEASE} "; \
      else \
        echo "No release found: ${RELEASE}"; \
      fi;
    - export TAG_NAME="debian/${DEB_VERSION}"
    - |
      if [ "${RELEASE_LENGTH}" -eq "0" ]; \
        then \
        echo "Creating release ${TAG_NAME} on branch ref ${CI_COMMIT_REF_NAME}"; \
        CREATE_RELEASE=`curl --header 'Content-Type: application/json' --header "${TOKEN}" \
          --data "{ \"id\": \"${CI_PROJECT_ID}\", \"name\": \"${DEB_VERSION}\", \"tag_name\": \"${TAG_NAME}\", \"ref\": \"${CI_COMMIT_REF_NAME}\" }" \
          --request POST "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/releases"`; \
        echo "Creating release ${TAG_NAME} result: ${CREATE_RELEASE}"; \
      else \
        echo "Using existing release ${TAG_NAME}"; \
      fi
    # Create of update release links with link to package
    - |
      RELEASE_LINKS=`curl --header "${TOKEN}" \
      "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/releases/${RELEASE_ID}/assets/links"`
    - RELEASE_LINKS_LENGTH=0
    - |
      if [[ "${RELEASE_LINKS}" != '{"message"'* ]]; \
        then \
        RELEASE_LINKS_LENGTH=`echo ${RELEASE_LINKS} | jq length`; \
        echo "Found ${RELEASE_LINKS_LENGTH} release links"; \
      else \
        echo "No release link found: ${RELEASE_LINKS}"; \
      fi;
    - export PACKAGE_URL="${CI_PROJECT_URL}/-/package_files/${PACKAGE_FILE_ID}/download"
    - |
      RELEASE_UPDATE=""; \
      if [ ${RELEASE_LINKS_LENGTH} -gt 0 ]; \
        then \
        echo "Parsing ${RELEASE_LINKS_LENGTH} existing release link(s) from ${RELEASE_LINKS}"; \
        RELEASE_LINK_INDEX=0; \
        RELEASE_LINK_ID=0; \
        while [ $RELEASE_LINK_INDEX -lt $((RELEASE_LINKS_LENGTH)) ]; \
        do \
          RELEASE_LINK_NAME=`echo ${RELEASE_LINKS} | jq -r .[${RELEASE_LINK_INDEX}].name`; \
          echo "Checking release link name ${RELEASE_LINK_NAME}"; \
          if [[ "${RELEASE_LINK_NAME}" == "${DATASETS_ARCHIVE}" ]]; \
            then \
            echo "Updating release link of ${DATASETS_ARCHIVE} to ${PACKAGE_URL}"; \
            RELEASE_LINK_ID=`echo ${RELEASE_LINKS} | jq -r .[${RELEASE_LINK_INDEX}].id`; \
            echo "Using release link id ${RELEASE_LINK_ID}"; \
            RELEASE_UPDATE=`curl --request PUT --header "${PRIVATE_TOKEN}" \
              --data id="${CI_PROJECT_PATH}" \
              --data tag_name="${TAG_NAME}" \
              --data name="${DATASETS_ARCHIVE_ESCAPED}" \
              --data url="${PACKAGE_URL}" \
              --data link_type="package" \
              "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/releases/${RELEASE_ID}/assets/links/${RELEASE_LINK_ID}"`; \
            break; \
          fi; \
          RELEASE_LINK_INDEX=$((${RELEASE_LINK_INDEX} + 1)); \
        done \
      fi; \
      if [[ "${RELEASE_UPDATE}" == "" ]]; \
        then \
        echo "Creating release link for ${DATASETS_ARCHIVE} to ${PACKAGE_URL}"; \
        RELEASE_UPDATE=`curl --request POST --header "${PRIVATE_TOKEN}" \
          --data id="${CI_PROJECT_PATH}" \
          --data tag_name="${TAG_NAME}" \
          --data name="${DATASETS_ARCHIVE_ESCAPED}" \
          --data url="${PACKAGE_URL}" \
          --data link_type="package" \
          "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/releases/${RELEASE_ID}/assets/links"`; \
      fi; \
      if [[ "${RELEASE_UPDATE}" == '{"message"'* ]]; \
        then \
        echo "Failed to update release: ${RELEASE_UPDATE}"; \
        exit 1; \
      fi
    # Trigger MPA pipeline
    - echo "Triggering MPA pipeline"
    - TRIGGER_PIPELINE=`curl --request POST --form "token=${TRIGGER_TOKEN}" --form ref=main "https://gitlab.com/api/v4/projects/${MPA_ID}/trigger/pipeline"`
    - |
      echo "Triggering MPA pipeline result: ${TRIGGER_PIPELINE}"
